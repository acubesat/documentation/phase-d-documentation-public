# AcubeSAT Phase D Documentation

This repository contains the public PDF files of the documents produced for AcubeSAT during Phase D.

These include test specifications, test reports and procedures, regarding AIV activities that took place in the Phase D of the mission

## Directories
- **TSTP**: Test Specifications and Test Procedures, including:
  - ADM_VIBE_TSTP.pdf (Antenna Deployment Mechanism Viberation Test)
  - ADM_TVAC_TSTP.pdf (Antenna Deployment Mechanism Thermal Vaccuum Test)
- **TRPT**: Test Reports, including:
  - ADM_TRPT.pdf (Antenna Deployment Mechanism Environmental Test Report)

## Disclaimers
The AcubeSAT project is carried out with the support of the Education Office of the [European Space Agency](https://www.esa.int/), under the educational [Fly Your Satellite!](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite/) programme.

All documents contain the analysis approaches, along with their justification and description, as well as the required documentation, all in compliance with the [European Space Agency](https://www.esa.int/) Policy.

All documents will remain open until the later stages of the project and therefore will be updated on a regular basis during the different [Fly Your Satellite! 3](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite) phases.

This repository has been authored by university students participating in the AcubeSAT project. The views expressed herein by the authors can in no way be taken to reflect the official opinion, or endorsement, of the [European Space Agency](https://www.esa.int/).

The documentation in this repository is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the documentation or the use or other dealings in the documentation.


